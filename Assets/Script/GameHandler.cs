﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    [SerializeField] private GameObject unitGameObjects;
    private IUnit unit;

    private void Awake()
    {
        unit = unitGameObjects.GetComponent<IUnit>();
    }
   private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Save();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Load();
        }
    }

    private void Save()
    {  //save
        Vector3 playerPosition = unit.GetPosition();   
        PlayerPrefs.SetFloat("playerPositionX", playerPosition.x);
        PlayerPrefs.SetFloat("playerPositionY", playerPosition.y);
        PlayerPrefs.SetFloat("playerPositionZ", playerPosition.z);
        PlayerPrefs.Save();
        Debug.Log("Saved");
    }
    private void Load()
    {
        //load
        if (PlayerPrefs.HasKey("playerPositionX"))
        {
            float playerPositionX = PlayerPrefs.GetFloat("playerPositionX");
            float playerPositionY = PlayerPrefs.GetFloat("playerPositionY");
            float playerPositionZ = PlayerPrefs.GetFloat("playerPositionZ");
            Vector3 playerPosition = new Vector3(playerPositionX, playerPositionY, playerPositionZ);

            unit.SetPosition(playerPosition);
        }
        else
        {
            Debug.Log("no save");
        }
    }
}
