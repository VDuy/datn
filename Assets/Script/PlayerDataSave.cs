﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PlayerData1
{
    public int money;
    public int health;
    public int level;
    public int attackDamage;
    public int mana;
    public int resistance;
    public float[] position;

    public PlayerData1()
    {
        money = 10000;
        health = PlayerData.instance.baseHP;
        level = PlayerData.instance.currentLevel;
        mana = PlayerData.instance.baseMP;
        attackDamage = PlayerData.instance.totalAtk;
        resistance = PlayerData.instance.totalDef;

        position = new float[3];
        position[0] = PlayerData.instance.transform.position.x;
        position[1] = PlayerData.instance.transform.position.y;
        position[2] = PlayerData.instance.transform.position.z;
    }
}

//public Data playerData;

//public void SaveGame()
//{
//    PlayerPrefs.SetString("data",JsonUtility.ToJson(playerData));
//}
//public void LoadGame()
//{
//    if (PlayerPrefs.HasKey("data"))
//    {//có data đã save rồi
//        playerData = JsonUtility.FromJson<Data>(PlayerPrefs.GetString("data"));
//    }
//    else
//    {//chưa có data save
//        playerData = new Data();
//    }

//}