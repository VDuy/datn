﻿using UnityEngine.AI;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    // Start is called before the first frame update
    public NavMeshAgent agent;
    public Transform player;
    public LayerMask whatIsGround, whatIsPlayer;


    public float walkPointRange;
    public Vector3 walkPoint;
    public bool walkPointSet;

    public int agentHP;

    public float attacksDelayTime;
    public bool isAttacked;

    public float sightRange;
    public float attackRange;
    public bool playerInSight;
    public bool playerInRange;


    public bool isChase = false;
    public static EnemyBehavior instance;
    private void Awake()
    {
        instance = this;
        player = GameObject.Find("You").transform;
        agent = GetComponent<NavMeshAgent>();

    }
    public void Start()
    {

    }
    private void Update()
    {
        playerInSight = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);



        if (playerInSight && !playerInRange && EnemyData.instance.enemyHP > 0)
        {
            Chase();

        }
        if (EnemyData.instance.enemyHP <= 0)
        {
            agent.isStopped = true;
        }
        if (playerInSight && playerInRange)
        {
            Attack();

        }
        if (!playerInSight && isChase)
        {
            isChase = false;
        }
    }


    private void Chase()
    {
        agent.SetDestination(player.position);

    }
    private void Attack()
    {
        agent.SetDestination(transform.position);
        transform.LookAt(player);
        if (!isAttacked)
        {
            EnemyData.instance.animController.SetBool("attack", true);
            isAttacked = true;
            Invoke("ResetAttack", 5f);
        }
    }
    private void ResetAttack()
    {
        isAttacked = false;
    }
}
